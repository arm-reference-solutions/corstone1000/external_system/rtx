# Arm Corstone-1000 External System RTX RTOS

---

__🚨Disclaimer:__

*_Arm reference solutions are Arm public example software projects that track and pull upstream components, incorporating their respective security fixes published over time._*
*_Users of these solutions are responsible for ensuring that the components they use contain all the required security fixes, if and when they deploy a product derived from Arm reference solutions._*

---

The [Corstone-1000 software stack][cs1000-latest-userguide] uses the [RTX Real-Time Operating System][arm-keil-rtx-info] as
the default operating system in its External System Cortex-M3 core.

It is built as part of the Yocto project [`meta-arm` layer][meta-arm-yocto-index].
The `meta-arm` [BitBake recipe][meta-arm-external-system-recipe] describes the default configuration of RTX.

## Prerequisites

### Tools
To properly build External system firmware for a target product, the following tools are
required:

- Git
- [GNU Arm Embedded Toolchain ("6-2017-q2-update" or later)](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm)
- GNU Make
- Python 3 (3.5.0 or later)

### CMSIS Libraries

External system firmware requires the use of components from the Cortex Microcontroller
System Interface Standard (CMSIS) Software Pack, specifically the CMSIS Core
and CMSIS Real-Time Operating System (RTOS) components. The CMSIS Software pack
is included as a Git submodule.

To retrieve the CMSIS Software pack, just initialize and update the submodule
of the repository.

```sh
$ git submodule update --init
```

The required CMSIS components are now present and the correct version has been
selected.

## Building Products

To build a product the basic command format for invoking 'make' is:

```sh
$ make [TARGET] <PRODUCT=<name>> [OPTIONS]
```

It is not necessary to provide a target since the default target for the product
will build all the firmware contained within the product.

The 'help' target provides further information on the arguments that can be
given:

```sh
$ make help
```

For more guidance on the build system, refer to the full set of documentation
that includes the 'Build System' chapter.

[cs1000-latest-userguide]: https://corstone1000.docs.arm.com/en/latest/user-guide.html
[arm-keil-rtx-info]: https://www.keil.com/arm/rl-arm/kernel.asp
[meta-arm-yocto-index]: https://git.yoctoproject.org/meta-arm/
[meta-arm-external-system-recipe]: https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp/recipes-bsp/external-system/external-system_0.1.0.bb
